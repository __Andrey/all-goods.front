import { computed } from 'mobx'
import { BaseRouter, _boolean, _number, _enum } from 'src/core/router'
import { Site, Page, Goods, Session, Result }   from 'src/core/goods'


export enum Section {
  Catalog     = 'catalog',
  Sites       = 'sites',
  Parsers     = 'parsers',
  Statistics  = 'statistics'
}

export enum Category {
  Smartphone  = 'smartphone',
  Laptop      = 'laptop',
  Monitor     = 'monitor',
}


function section_reset(state_name) {
  if (state_name === 'category') 
    return false 
  return true
}

function category_reset(state_name) {
  if (state_name === 'section') 
    return false 
  return true
}

function session_reset(state_name) {
  if (state_name === 'section'
  ||  state_name === 'category') 
    return false 
  return true
}

function result_reset(state_name) {
    return false 
}


class Router extends BaseRouter {
  log = true
  // @_type(reset_options) value_name = default value'
  @_enum    (section_reset , Section  ) section        : Section  = Section.Catalog 
  @_enum    (category_reset, Category ) category       : Category = Category.Smartphone 
  @_number  (session_reset  ) session_id     : number | null = null
  @_number  (result_reset   ) result_id      : number | null = null

  @computed get session() : Session { return (Session.get(this.session_id) as Session) }
  @computed get result () : Result  { return (Result .get(this.result_id ) as Result ) }
}

let router = new Router() 
router.init()
export default router
