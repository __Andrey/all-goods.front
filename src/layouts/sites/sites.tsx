import React        from 'react'
import { observer } from 'mobx-react'
import Layout from 'src/core/layout'


@observer
export default class LayoutSites extends Layout {
    render() {
        return (
            <div className='layout-sites'>
                Sites
            </div>
        )
    }
}
