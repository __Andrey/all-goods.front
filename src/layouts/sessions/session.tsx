import './session.styl'
import React        from 'react'
import { Route    } from 'react-router'
import { observer } from 'mobx-react'
import router       from 'src/router'
import Layout       from 'src/core/layout'
import { Result, ResultNavigation, SessionDetails } from 'src/core/goods'
import LayoutResult from './result'


@observer
export default class LayoutSessionDetails extends Layout {

  async load() : Promise<any> {
    return Promise.all([
      Result.load({'session_id': {'==': router.session_id}})
    ])
  }

  render() {
    if (this.is_loading) return (<div>Loading...</div>)
    else {
      if (!router.session) 
        return null
      return (
        <div className='layout-session'>
          <SessionDetails   session={router.session}/>
          <div className='layout-session__results'>
            <ResultNavigation results={router.session.results}/>
            <Route path={`${this.props.match.path}/result_id/:result_id`} component={LayoutResult} />
          </div>
        </div>
      )
    }
  }
}
