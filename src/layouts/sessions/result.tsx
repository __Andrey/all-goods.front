import React             from 'react'
import { observer }      from 'mobx-react'
import router            from 'src/router'
import Layout            from 'src/core/layout'
import { ResultDetails } from 'src/core/goods'


@observer
export default class LayoutResult extends Layout {
  render() {
    if (!router.result) 
        return null
    return (
        <div className='layout-result'>
            <ResultDetails result={router.result}/>
        </div>
    )
  }
}
