import './sessions.styl'
import React        from 'react'
import { Route    } from 'react-router'
import { computed } from 'mobx'
import { observer } from 'mobx-react'
import { Site, Page, Session, Goods, SessionNavigation  } from 'src/core/goods'
import LayoutSession from './session'
import Layout from 'src/core/layout'
import * as smartphone from 'src/core/goods/category/smartphone'


@observer
export default class LayoutSessions extends Layout {

  @computed get sessions() : Session[] { return Session.all() as Session[] }

  async load(): Promise<any> {
    return Promise.all([
      Site   .load(),
      Page   .load(),
      Session.load(),
      Goods  .load(),
      smartphone.Smartphone.load()
    ])
  }

  render() {
    if (this.is_loading) return (<div className='layout-sessions'>Loading...</div>)
    else {
      return (
        <div className='layout-sessions'>
          <SessionNavigation sessions={this.sessions}></SessionNavigation>
          <Route path={`${this.props.match.path}/session_id/:session_id`} component={LayoutSession} />
        </div>
      )
    }
  }
}
