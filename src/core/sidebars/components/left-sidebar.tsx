import './left-sidebar.styl'
import React from 'react'
import { observer } from 'mobx-react'
import sidebars from '../index'
import List         from '@material-ui/core/List'
import ListItem     from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Divider      from '@material-ui/core/Divider'
import InboxIcon    from '@material-ui/icons/Inbox'
import DraftsIcon   from '@material-ui/icons/Drafts'
import AssessmentIcon   from '@material-ui/icons/Assessment'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import VisibilityIcon   from '@material-ui/icons/Visibility'
import PlaceIcon        from '@material-ui/icons/Place'
import PhoneAndroidIcon from '@material-ui/icons/PhoneAndroid'
import router, { Section, Category } from 'src/router'


@observer
class LeftSidebar extends React.Component {

    selectItem(event, section) {
        router.section = section
    }

    selectCategory(event, category) {
        router.category = category
    }

    render() {
        return (
            <div className='left-sidebar'>
                <button onClick={()=> {sidebars.left.toggle()}}>Sidebar</button>
                <Divider/>
                <List component='nav'>
                    <ListItem button selected={ router.section === Section.Statistics } onClick={event => this.selectItem(event, Section.Statistics)}>
                        <ListItemIcon>
                            <AssessmentIcon />
                        </ListItemIcon>
                        <ListItemText primary='Statistic'/>
                    </ListItem>
                    <ListItem button selected={ router.section === Section.Parsers } onClick={event => this.selectItem(event, Section.Parsers)}>
                        <ListItemIcon>
                            <VisibilityIcon />
                        </ListItemIcon>
                        <ListItemText primary='Parser'/>
                    </ListItem>
                    <ListItem button selected={ router.section === Section.Catalog } onClick={event => this.selectItem(event, Section.Catalog)}>
                        <ListItemIcon>
                            <ShoppingCartIcon />
                        </ListItemIcon>
                        <ListItemText primary='Catalog'/>
                    </ListItem>
                    <ListItem button selected={ router.section === Section.Sites } onClick={event => this.selectItem(event, Section.Sites)}>
                        <ListItemIcon>
                            <PlaceIcon />
                        </ListItemIcon>
                        <ListItemText primary='Sites'/>
                    </ListItem>
                </List>
                <Divider/>
                <List component='nav'>
                    <ListItem button selected={ router.category === Category.Smartphone } onClick={event => this.selectCategory(event, Category.Smartphone)}>
                        <ListItemIcon>
                            <PhoneAndroidIcon />
                        </ListItemIcon>
                        <ListItemText primary='Smartphone'/>
                    </ListItem>
                    <ListItem button selected={ router.category === Category.Laptop } onClick={event => this.selectCategory(event, Category.Laptop)}>
                        <ListItemIcon>
                            <PhoneAndroidIcon />
                        </ListItemIcon>
                        <ListItemText primary='Laptop'/>
                    </ListItem>
                    <ListItem button selected={ router.category === Category.Monitor } onClick={event => this.selectCategory(event, Category.Monitor)}>
                        <ListItemIcon>
                            <PhoneAndroidIcon />
                        </ListItemIcon>
                        <ListItemText primary='Monitor'/>
                    </ListItem>
                </List>
            </div>
        )
    }
}

export default LeftSidebar
