import { computed, observable, action }           from 'mobx'
import { diff } from 'jsondiffpatch'
import { Model, model, id, field, foreign, many, datetime, number, boolean } from 'mobx-orm'
import { rest } from 'src/core/rest'


@model
@rest('goods-site')
export class Site extends Model {
    @id    id   : number
    @field url  : string
    @field desc : string

    @many('Page' , 'site_id') pages: Page[]
    @many('Goods', 'site_id') goods: Goods[]
}

@model
@rest('goods-page')
export class Page extends Model {
    @id         id         : number
    @field      url        : string
    @datetime   last_visit : Date
    @number     site_id    : number

    @foreign(Site) site : Site

    @many('Result', 'page_id') results: Result[]
}

@model
@rest('goods-goods')
export class Goods extends Model {
    @id         id             : number
    @number     site_id        : number
    @field      idx            : string
    @datetime   last_updated   : Date
    @field      category       : string
    @number     link_id        : number
    @field      title          : string
    @field      desc           : any
    @field      price          : any

    @foreign(Site) site : Site

    @many('Result', 'goods_id') results: Result[]
}

@model
@rest('goods-session')
export class Session extends Model {
    @id         id         : number
    @field      spider     : string
    @datetime   started    : Date
    @datetime   finished   : Date
    @boolean    checked    : boolean

    @many('Result', 'session_id') results: Result[]

    @computed({keepAlive: true}) get count_new_items() {
        let count = 0
        for(let result of this.results)
            if (result.is_new)
                count++
        return count
    }

    @computed({keepAlive: true}) get count_changed_items() {
        let count = 0
        for(let result of this.results)
            if (result.is_changed)
                count++
        return count
    }
}

@model
@rest('goods-result')
export class Result extends Model {
    @id    id         : number
    @datetime   timestamp  : Date
    @number     session_id : number
    @number     page_id    : number
    @number     goods_id   : number
    @field      title      : string
    @field      desc       : any
    @field      price      : any

    @foreign(Session) session : Session
    @foreign(Page   ) page    : Page
    @foreign(Goods  ) goods   : Goods

    @computed({keepAlive: true}) get desc_delta() { return this.goods ? diff(this.goods.desc, this.desc) : {} }
    @computed({keepAlive: true}) get is_new    () { return false }
    @computed({keepAlive: true}) get is_changed() { return !!this.desc_delta }

    @action async updateGoods() {
        this.goods.desc  = this.desc
        this.goods.price = this.price
        this.goods.last_updated = this.timestamp 
        await this.goods.save()
    }
}
