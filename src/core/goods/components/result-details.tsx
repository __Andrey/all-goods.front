import React from 'react'
import { observer } from 'mobx-react'
import { formatters } from 'jsondiffpatch'
import 'jsondiffpatch/dist/formatters-styles/html.css'
import { Result } from '../models'
import GoodsDetails from './goods-details' 
import Button from '@material-ui/core/Button';
import './result-details.styl'


interface ResultDetailsProps {
    result: Result 
}

@observer
export default class ResultDetails extends React.Component<ResultDetailsProps> {
  render() {
    let result = this.props.result
    return (
      <div className='result-details block'>
        <div className='result-details-info'>
            <div>
                <div>Result Details</div>
                <div>Id:        {`${result.id}`}</div>
                <div>Timestamp: {`${result.timestamp}`}</div>
                {/* <div>Desc:      {JSON.stringify(result.desc)}</div> */}
                {/* <div>Price:     {JSON.stringify(result.price)}</div> */}
            </div>
            <div dangerouslySetInnerHTML={ {__html: formatters.html.format(result.desc_delta, result.desc)}}/>
            <Button onClick={()=> result.updateGoods()} style={{display: result.desc_delta ? 'block': 'none'}}>Update goods</Button>
        </div>
        <GoodsDetails id={result.goods_id}/>
      </div>
    )
  }
}
