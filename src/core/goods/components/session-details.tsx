import React from 'react'
import { observer } from 'mobx-react'
import { Session } from '../models'
import './session-details.styl'

interface SessionDetailsProps {
    session: Session 
}

@observer
export default class SessionDetails extends React.Component<SessionDetailsProps> {
    render() {
        debugger
        return(
        <div className='session-details block'>
            <div>
                <div> Id: {`${this.props.session.id}`}</div>
                <div> Spider: {`${this.props.session.spider}`}</div>
                <div> Started: {`${this.props.session.started}`}</div>
                <div> Finished: {`${this.props.session.finished}`}</div>
            </div>
            <div>
                <div> Checked: {`${this.props.session.checked}`}</div>
                <div> All Items:     {`${this.props.session.results.length}`}</div>
                <div> New Items:     {`${this.props.session.count_new_items}`}</div>
                <div> Changed Items: {`${this.props.session.count_changed_items}`}</div>
            </div>
        </div>
        )
    }
}
