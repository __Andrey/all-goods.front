import React from 'react'
import { observer } from 'mobx-react'
import { Goods } from '../models'
import * as smartphone from '../category/smartphone'
import './goods-details.styl'


interface GoodsDetailsProps {
    id: number 
}

enum Mode {
    NOTHING,
    IS_CREATING,
    IS_CHOOSING
}

interface GoodsDetailsState {
    goods: Goods | null
    mode: Mode
}

@observer
export default class GoodsDetails extends React.Component<GoodsDetailsProps, GoodsDetailsState> {
    constructor(props) {
        super(props)
        this.state = {mode : Mode.NOTHING, goods: Goods.get(props.id) as Goods}
        this.attachCategoryGoods = this.attachCategoryGoods.bind(this)
    }

    async attachCategoryGoods(category_goods) {
        this.state.goods.link_id = category_goods.id
        await this.state.goods.save()
        this.setState({mode: Mode.NOTHING})
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.state.goods.id != this.props.id)
            this.setState({goods: Goods.get(this.props.id) as Goods})
        if(this.props.id != prevProps.id)
            this.setState({mode: Mode.NOTHING})
    }

    render() { 
        let goods = this.state.goods
        return (
            <div className='goods-details block'>
                <div>
                    <div>Goods Details</div>
                    <div>Id:        {`${goods.id       }`}</div>
                    <div>Idx:       {`${goods.idx      }`}</div>
                    <div>Category:  {`${goods.category }`}</div>
                    <div>Link Id:   {`${goods.link_id  }`}</div>
                    <div>Last Updated: {`${goods.last_updated }`}</div>
                    {/* <div>Desc:      {JSON.stringify(goods.desc)}</div> */}
                    {/* <div>Price:     {JSON.stringify(goods.price)}</div> */}
                </div>
                <div>
                    <button onClick={()=> this.setState({mode: Mode.IS_CHOOSING})}>Choose</button>
                    <button onClick={()=> this.setState({mode: Mode.IS_CREATING})}>Create</button>
                </div>
                { this.state.mode == Mode.IS_CREATING ? <smartphone.SmartphoneCreate cls={smartphone.Smartphone} onCreated={this.attachCategoryGoods} default={goods.desc}/> : null }
                { this.state.mode == Mode.IS_CHOOSING ? <smartphone.SmartphoneChoose onChoosen={this.attachCategoryGoods}/> : null }
                { this.state.mode == Mode.NOTHING &&  goods.link_id ? <smartphone.SmartphoneDetails id={goods.link_id}/> : null }
                { this.state.mode == Mode.NOTHING && !goods.link_id ? <div> Not linked </div> : null }
            </div>
        )
    }
}
