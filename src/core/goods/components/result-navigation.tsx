import './result-navigation.styl'
import React                  from 'react'
import { NavLink }            from 'react-router-dom'
import { observer, Observer } from 'mobx-react'
import TextField    from '@material-ui/core/TextField'
import List         from 'react-virtualized/dist/commonjs/List'
import AutoSizer    from 'react-virtualized/dist/commonjs/AutoSizer'
import router       from 'src/router'
import { Result }   from '../models'


interface ResultNavigationProps {
    results: Result[] 
}

interface ResultNavigationState {
    search  : string 
    results : Result[]
}

@observer
export default class ResultNavigation extends React.Component<ResultNavigationProps, ResultNavigationState> {

    constructor(props) {
        super(props)
        this.state = {search : '', results: this.filter_results()}
        this.onSearchChange = this.onSearchChange.bind(this)
        this.rowRenderer = this.rowRenderer.bind(this)
    }

    onSearchChange(event) {
      this.setState({search: event.target.value})
      this.setState({results: this.filter_results(event.target.value)}) 
    }

    filter_results(search: string = '') : Result[] {
        let results = []
        if (this.state && this.state.search.length) {
            results = this.props.results.filter((result: Result) => {
                return result.title.toUpperCase().includes(search.toUpperCase())
                    || String(result.id) == search
            })
        }
        else {
            results = this.props.results
        }
        return results
    }
    rowRenderer ({key, index, style}) {
        return (
            <Observer key={key}>
                {() => this.renderItems(index, key, style)}
            </Observer>
        )
    }

    renderItems(index, key, style) {
        let result = this.state.results[index]
        return (
            <div key={key} style={style} className={`result-navigation__list-item ${result.is_new ? 'is_new': ''} ${result.is_changed ? 'is_changed': ''}`}>
                <NavLink to={router.generateUrl({result_id: result.id})}>
                    {`${result.id} - ${result.title}`}
                </NavLink>
            </div>
        )
    }

    render() {
        return (
            <div className='result-navigation block'>
                <TextField label='search' value={this.state.search} onChange={this.onSearchChange} />
                <div className='result-navigation__list'>
                    <AutoSizer>
                    {({ height, width }) => (
                        <List 
                            width      ={width} 
                            height     ={height}
                            rowCount   ={this.state.results.length}
                            rowHeight  ={25}
                            rowRenderer={this.rowRenderer} /> 
                    )}
                    </AutoSizer>
                </div>
            </div>
        )
    }
}
