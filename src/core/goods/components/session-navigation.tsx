import './session-navigation.styl'  
import   React      from 'react'
import { observer, Observer } from 'mobx-react'
import TextField    from '@material-ui/core/TextField'
import List         from 'react-virtualized/dist/commonjs/List'
import AutoSizer    from 'react-virtualized/dist/commonjs/AutoSizer'
import { NavLink  } from 'react-router-dom'
import   router     from 'src/router'
import { Session  } from '../models'


interface SessionNavigationProps {
    sessions: Session[] 
}

interface SessionNavigationState {
    search   : string 
    sessions : Session[]
}

@observer
export default class SessionNavigation extends React.Component<SessionNavigationProps, SessionNavigationState> {
    constructor(props) {
        super(props)
        this.state = {search : '', sessions: this.filter_sessions()}
        this.onSearchChange = this.onSearchChange.bind(this)
        this.rowRenderer = this.rowRenderer.bind(this)
    }

    onSearchChange(event) {
        this.setState({search: event.target.value})
        this.setState({sessions: this.filter_sessions(event.target.value)}) 
    }

    filter_sessions(search: string = '') : Session[] {
        let sessions = []
        if (this.state && this.state.search.length) {
            sessions = this.props.sessions.filter((result: Session) => {
                return result.spider.toUpperCase().includes(search.toUpperCase())
            })
        }
        else {
            sessions = this.props.sessions
        }
        return sessions
    }

    rowRenderer ({key, index, style}) {
        return (
            <Observer key={key}>
                {() => this.renderItems(index, key, style)}
            </Observer>
        )
    }

    renderItems(index, key, style) {
        let session = this.state.sessions[index]
        return (
            <div key={key} style={style} className={`session-navigation__list-item`}>
                <NavLink to={router.generateUrl({session_id: session.id})}>
                    {`${session.id} - ${session.spider.split(':')[1]}`}
                </NavLink>
            </div>
        )
    }

  render() {
    return (
        <div className='session-navigation block'>
            <TextField label='search' value={this.state.search} onChange={this.onSearchChange} />
            <div className='session-navigation__list'>
                <AutoSizer>
                {({ height, width }) => (
                    <List 
                        width      ={width} 
                        height     ={height}
                        rowCount   ={this.state.sessions.length}
                        rowHeight  ={25}
                        rowRenderer={this.rowRenderer} /> 
                )}
                </AutoSizer>
            </div>
        </div>
    )
  }
}
