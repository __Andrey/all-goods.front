import React from 'react'
import { observer } from 'mobx-react'


interface SmartphoneCreateProps {
    onCreated : (object: any) => any
    cls       : any  
    default  ?: any 
}

let names = {
}

@observer
export default class SmartphoneCreate extends React.Component<SmartphoneCreateProps> {
    private obj: any

    constructor(props) {
        super(props)
        this.obj          = new props.cls(props.default)
        this.onSubmit     = this.onSubmit    .bind(this)
        this.onFormChange = this.onFormChange.bind(this)
    }

    onFormChange(event) {
        this.obj[event.target.name] = event.target.value
    }

    async onSubmit(event) {
        event.preventDefault()
        await this.obj.save()
        this.props.onCreated(this.obj)
    }

    render() { 
        let inputs = []
        let fields_desc = this.props.cls.getFieldsNames()
        for(let field_name of Object.keys(fields_desc)) {
            if (fields_desc[field_name].type === 'field') {
                inputs.push(
                    <label key={field_name}> { names[field_name] ? names[field_name] : field_name }:
                        <input type='text' name={field_name} value={this.obj[field_name]} onChange={this.onFormChange} />
                    </label>
                )
            }
        }

        return ( 
            <div className='smartphone-create block'>
                <div>Smartphone Create</div>
                <form onSubmit={this.onSubmit}>
                    { inputs }
                    <input type='submit' value='Submit' />
                </form>
            </div>
        )
    }
}
