import React          from 'react'
import { observer   } from 'mobx-react'
import { Smartphone } from '../models'


interface SmartphoneDetailsProps {
    id         ?: number
    smartphone ?: Smartphone 
}

@observer
export default class SmartphoneDetaisl extends React.Component<SmartphoneDetailsProps> {
    render() { 
        let smartphone
             if (this.props.smartphone) smartphone = this.props.smartphone
        else if (this.props.id        ) smartphone = Smartphone.get(this.props.id)
        if (smartphone)
            return (
                <div className='smartphone-details block'>
                    <div>Smartphone Details</div>
                    <div>Id  : {`${smartphone.id}  `}</div>
                    <div>Name: {`${smartphone.name}`}</div>
                </div>
            )
        else
            return <div className='smartphone-details block'>Nothing {`${this.props.id} - ${this.props.smartphone}`}</div>
    }
}
