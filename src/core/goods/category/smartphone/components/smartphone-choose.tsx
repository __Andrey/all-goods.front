import './smartphone-choose.styl'
import React from 'react'
import { computed } from 'mobx'
import { observer } from 'mobx-react'
import { Smartphone } from '../models'
import TextField    from '@material-ui/core/TextField'
import List         from 'react-virtualized/dist/commonjs/List'
import AutoSizer    from 'react-virtualized/dist/commonjs/AutoSizer'
import SmartphoneDetails from './smartphone-details'


interface SmartphoneChooseProps {
    onChoosen: (smartphone: Smartphone) => any
}

interface SmartphoneChooseState {
    search              : string 
    smartphones         : Smartphone[]
    smartphone_hovered  : Smartphone | null
}


@observer
export default class SmartphoneChoose extends React.Component<SmartphoneChooseProps, SmartphoneChooseState> {
    root_element: any 

    constructor(props) {
        super(props)
        this.state = {search : '', smartphones: this.filter_smartphones(), smartphone_hovered: null}
        this.onSearchChange = this.onSearchChange.bind(this)
        this.onHover        = this.onHover.bind(this)
        this.rowRenderer    = this.rowRenderer.bind(this)
    }

    onSearchChange(event) {
      this.setState({search: event.target.value})
      this.setState({smartphones: this.filter_smartphones(event.target.value)}) 
    }

    onHover(smartphone) {
      this.setState({smartphone_hovered: smartphone})
    }

    filter_smartphones(search: string = '') : Smartphone[] {
        console.log(search)
        let smartphones = (Smartphone.all() as Smartphone[])
        if (this.state && this.state.search.length) {
            smartphones = smartphones.filter((smartphone) => {
                return smartphone.title.toUpperCase().includes(search.toUpperCase())
            })
        }
        return smartphones
    }

    rowRenderer ({key, index, style}) {
        let item = this.state.smartphones[index]
        return (
            <div key={key} className='smartphone-choose__list-item' style={style} onClick={()=> this.props.onChoosen(item)} onMouseEnter={()=> this.onHover(item)}>
                {`Id: ${item.id} Name: ${item.title}`}
            </div>
        )
    }

    render() { 
        return (
            <div className='smartphone-choose block'>
                <div>
                    <TextField label='search' value={this.state.search} onChange={this.onSearchChange} />
                    <div className='smartphone-choose__list'>
                        <List 
                            width      ={300} 
                            height     ={400}
                            rowCount   ={this.state.smartphones.length}
                            rowHeight  ={25}
                            rowRenderer={this.rowRenderer} /> 
                        {/* <AutoSizer>
                        {({ height, width }) => (
                            <List 
                                width      ={300} 
                                height     ={400}
                                rowCount   ={this.state.smartphones.length}
                                rowHeight  ={25}
                                rowRenderer={this.rowRenderer} /> 
                        )}
                        </AutoSizer> */}
                    </div>
                </div>
                <SmartphoneDetails smartphone={this.state.smartphone_hovered}/>
            </div>
        )
    }
}
