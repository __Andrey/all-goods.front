import { computed, observable, action }           from 'mobx'
import { Model, model, id, field, foreign, many } from 'mobx-orm'
import { rest } from 'src/core/rest'


@model
@rest('goods-smartphone')
export class Smartphone extends Model {
  @id    id    : number
  @field title : string = ''
}