import SmartphoneDetails from './components/smartphone-details'
import SmartphoneCreate  from './components/smartphone-create'
import SmartphoneChoose  from './components/smartphone-choose'
import { Smartphone } from './models'
export {
    Smartphone, 

    SmartphoneDetails,
    SmartphoneCreate,
    SmartphoneChoose
}